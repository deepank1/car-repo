
let inventory = require("./car_details.js");


function problem3(inventory) {
    let car_model_arr = [];

    if(Array.isArray(inventory)){
        for(let i=0;i<inventory.length;i++){
            car_model_arr.push(inventory[i].car_model);
        }
        car_model_arr.sort((a,b)=>a.localeCompare(b));
        return car_model_arr;
    }
    return [];
}

module.exports = problem3;