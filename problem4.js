
let inventory = require("./car_details.js");

function problem4(inventory) {
    if(Array.isArray(inventory)){
        let all_cars_years = [];
        for(let i=0;i<inventory.length;i++){
            all_cars_years.push(inventory[i].car_year);
        }
        return all_cars_years;
    }
    return [];
}

module.exports = problem4;