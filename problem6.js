
let inventory = require("./car_details.js");

function problem6(inventory) {
    if(Array.isArray(inventory)){
        if(inventory.length===0){
            return [];
        }
        let bmw_audi_arr = []

        for(let i=0;i<inventory.length;i++){
            if(inventory[i].car_make.toLowerCase()=="bmw" || inventory[i].car_make.toLowerCase()=="audi"){
                bmw_audi_arr.push(inventory[i])
            }
        }
        return bmw_audi_arr;
    }
    return [];
}

module.exports = problem6;