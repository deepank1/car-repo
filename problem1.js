
let inventory = require("./car_details.js");

function problem1(inventory, required_car_id) {

    if(Array.isArray(inventory)){
        if(required_car_id===undefined || inventory.length===0){
            return [];
        }
        if(Number.isInteger(required_car_id)){
            for(let i=0;i<inventory.length;i++){
                if(inventory[i].id==required_car_id){
                    return inventory[i];
                }
            }
        }
    }
    return [];
}

module.exports = problem1;