
let inventory = require("./car_details.js");

function problem2(inventory) {
    if(Array.isArray(inventory)){
        if(inventory.length===0){
            return [];
        }
        return inventory[inventory.length-1];
    }
    return []
}

module.exports = problem2;