
let inventory = require("./car_details.js");
const problem4 = require("./problem4.js");

function problem5(inventory) {
    if(Array.isArray(inventory)){
        if(inventory.length===0){
            return [];
        }
        let all_cars_years_arr = problem4(inventory);
        let older_car_arr = [];

        for(let i=0;i<all_cars_years_arr.length;i++){
            if(all_cars_years_arr[i]<2000){
                older_car_arr.push(all_cars_years_arr[i]);
            }
        }
        return older_car_arr;
    }
    return [];
}

module.exports =  problem5;