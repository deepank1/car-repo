let required_car_id = "33";

let inventory = require("./car_details.js");
let problem1 = require("./problem1.js");

let result = problem1(inventory, required_car_id)


if(result.length===0){
    console.log("Inappropriate data")
}else{
    console.log("Car "+result.id + " is a "+ result.car_year + " " +result.car_make + " " +result.car_model);
}